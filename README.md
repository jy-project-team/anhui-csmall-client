# 皖派商城项目

文件名：anhui-csmall-client

## 一、部署

### 1.加载依赖
```
npm install
```

### 2.启动
```
npm run serve
```

### 3.构建
```
npm run build
```

## 二、介绍

该项目主要分为三大页面：

- 登录页面
- 前台商品页面
- 后台管理页面

### 1.登录页面：

登录页面中包含：

- 普通用户登录`LoginView.vue`
- 普通用户注册`RegisterView.vue`
- 管理员登录`AdminLoginView.vue`

### 2.前台商品页面：

前台商品页面包含：

- 商品主页`IndexView.vue`
- 个人管理`UserIndexView.vue`
  - 待付款`WaitToPay.vue`
  - 待发货`WaitToDistribute.vue`
  - 待收货`WaitToTake.vue`
  - 待评价(暂未实现...)
  - 退货`WaitToBack.vue`
- 个人详情页面`UserDetailView.vue`
- 个人地址管理`UserAddressView.vue`
- 个人购物车管理`ShoppingCartView.vue`
- 商品详情信息`ProductDetailView.vue`
- 商品订单信息`ProductOrderView.vue`
- 商品付款成功页面`PaySuccessView.vue`
- 商品搜索页面`SearchResultView.vue`

### 3.后台管理页面:

后台管理页面包含：

#### 主页：

- 后台主页`ManageView.vue`
- 后台首页`SystemAdminIndex.vue`

#### 账号管理：

- 管理员管理：
  - 管理员列表`AdminListNew.vue`
  - 管理员新增`AdminAddNewView.vue`
- 用户管理：
  - 用户列表`UserListView.vue`
- 收货地址管理：
  - 收货地址列表

#### 商品管理：

- 新增SPU
  - 选择类别`SpuAddNewStep1View.vue`
  - 填写基本信息`SpuAddNewStep2View.vue`
  - 选择相册`SpuAddNewStep3View.vue`
  - 确认商品详情`SpuAddNewStep4View.vue`
  - 等待审核`SpuFinishWaitView.vue`
- 轮播图管理
  - 轮播图列表`BannerListView.vue`
  - 添加轮播图`BannerAddNewView.vue`
- 相册管理
  - 相册列表`AlbumListView.vue`
  - 添加相册`AlbumAddNewView.vue`
  - 图片列表`PictureListView.vue`
  - 添加图片`PictureAddNew.vue`
- 商品类别管理
  - 类别列表`CategoryListView.vue`
  - 添加类别`CategoryAddNew.vue`
- 属性管理
  - 属性模板列表`AttributeTemplateListView.vue`
  - 添加属性模板`AttributeTemplateAddNewView.vue`
  - 属性列表`AttributeListView.vue`
  - 添加属性`AttributeAddNewView.vue`
- 品牌管理
  - 品牌列表`BrandListView.vue`
  - 添加品牌`BrandAddNewView.vue`

#### 订单管理：

- 用户订单-未发货`OrderListToNotDistribute.vue`
- 用户订单-已发货`OrderListToDistribute.vue`
- 用户订单-已退货`OrderListToBack.vue`

#### 营销管理：

- 待审查Spu列表`SpuListToCheckView.vue`
- 已上架Spu列表`SpuListToCheckPublishView.vue`
