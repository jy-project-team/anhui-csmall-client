import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import axios from 'axios';
import qs from 'qs';
import wangEditor from "wangeditor";
import * as echarts from 'echarts'
import global from '../src/components/Global'; // 导入设置的全局变量

// 引入Echarts
Vue.prototype.$echarts = echarts;
// 引入全局变量
Vue.prototype.GLOBAL = global; // 赋值给Vue
// 引入富文本编辑器
Vue.prototype.wangEditor = wangEditor;// 红色的wangEditor是this打点调用的名字
// 引入前端组件Element-UI
Vue.use(ElementUI);
Vue.config.productionTip = false
// 引入QS框架(用于将对象进行JSON序列化)
Vue.prototype.qs = qs;
// 引入AXIOS框架(封装XHR)用于进行网络请求
Vue.prototype.axios = axios;//添加Axios框架发送请求

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
